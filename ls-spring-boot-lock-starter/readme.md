## maven打包
mvn install:install-file 
-Dfile=ls-spring-boot-lock-starter-1.0.0-SNAPSHOT.jar 
-DgroupId=com.starter 
-DartifactId=ls-spring-boot-lock-starter 
-Dversion=1.0.0-SNAPSHOT 
-Dpackaging=jar

## 该项目是做了一个注解类，供开发者使用



## 注解开发
+ 1.@Documented：用于标记在生成javadoc时是否将注解包含进去，可以看到这个注解和@Override一样，注解中空空如也，什么东西都没有
+ 2.@Retention(RetentionPolicy.RUNTIME) 运行时启作用
+ 3.@Target()
+ 4.@Inherited 可继承





### 面向切面编程的注解
+ 1.@Aspect:作用是把当前类标识为一个切面供容器读取

+ 2.@Pointcut：Pointcut是植入Advice的触发条件。每个Pointcut的定义包括2部分，一是表达式，二是方法签名。
  方法签名必须是 public及void型。可以将Pointcut中的方法看作是一个被Advice引用的助记符，因为表达式不直观，因此我们可以通过方法签名的方式为此表达式命名。
  因此Pointcut中的方法只需要方法签名，而不需要在方法体内编写实际代码。
+ 3.@Around：环绕增强，相当于MethodInterceptor
+ 4.@AfterReturning：后置增强，相当于AfterReturningAdvice，方法正常退出时执行
+ 5.@Before：标识一个前置增强方法，相当于BeforeAdvice的功能，相似功能的还有
+ 6.@AfterThrowing：异常抛出增强，相当于ThrowsAdvice
+ 7.@After: final增强，不管是抛出异常或者正常退出都会执行