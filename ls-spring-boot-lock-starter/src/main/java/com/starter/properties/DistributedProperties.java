package com.starter.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * Redisson配置映射类
 * @author guanxiaop
 * @date 2022/1/26 14:22
 */
@ConfigurationProperties(prefix = "distributed.lock.server")
public class DistributedProperties {

    /**
     * redis主机地址，ip：port
     */
    private String address = "redis://localhost:6379";
    /**
     * redis连接密码
     */
    private String password;
    /**
     * 选取那个数据库
     */
    private int database = 0;
    /**
     * 超时时间
     */
    private int timeout = 30000;
    /**
     * 连接池数量
     */
    private int connectionPoolSize = 150;
    /**
     * 最小连接数
     */
    private int connectionMinIdleSize = 10;

    public String getPassword() {
        return password;
    }

    public DistributedProperties setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getDatabase() {
        return database;
    }

    public DistributedProperties setDatabase(int database) {
        this.database = database;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }

    public int getConnectionMinIdleSize() {
        return connectionMinIdleSize;
    }

    public void setConnectionMinIdleSize(int connectionMinIdleSize) {
        this.connectionMinIdleSize = connectionMinIdleSize;
    }
}
