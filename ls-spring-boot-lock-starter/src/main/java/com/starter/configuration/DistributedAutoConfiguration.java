package com.starter.configuration;

import com.starter.advice.DistributedLockAspect;
import com.starter.properties.DistributedProperties;
import com.starter.service.impl.DistributedLockServiceImpl;
import org.redisson.Redisson;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author guanxiaop
 * @date 2022/1/26 14:21
 */
@Configuration
@ConditionalOnClass(Redisson.class)
@EnableConfigurationProperties(DistributedProperties.class)
@Import({DistributedLockServiceImpl.class, DistributedLockAspect.class})
public class DistributedAutoConfiguration {
}
