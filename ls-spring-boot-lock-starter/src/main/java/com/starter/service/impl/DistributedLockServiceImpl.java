package com.starter.service.impl;

import com.starter.service.DistributedLockService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author：guan
 * @Date：2022-1-15 22:09
 */
@Service
@Slf4j
public class DistributedLockServiceImpl implements DistributedLockService {

    @Resource
    private RedissonClient redissonClient;

    @Override
    public boolean lock(String lockName, long expireSeconds) {
        RLock rLock = redissonClient.getLock(lockName);
        boolean lock;
        try {
            lock = rLock.tryLock(0, expireSeconds, TimeUnit.SECONDS);
            if (lock) {
                log.info("获取Redisson分布式锁[成功],lockName={}", lockName);
            } else {
                log.info("获取Redisson分布式锁[失败],lockName={}", lockName);
            }
        } catch (InterruptedException e) {
            log.info("获取Redisson分布式锁[异常]，lockName=" + lockName, e);
            e.printStackTrace();
            return false;
        }
        return lock;
    }

    @Override
    public void release(String lockName) {
        RLock lock = redissonClient.getLock(lockName);
        if(lock.isLocked() && lock.isHeldByCurrentThread()){
            lock.unlock();
        }
    }
}
