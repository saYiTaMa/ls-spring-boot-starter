package com.starter.service;

/**
 * 分布式锁的接口
 *
 * @author：guan
 * @Date：2022-1-15 22:08
 */
public interface DistributedLockService {

    /**
     * 加锁
     *
     * @param lockName      锁名称
     * @param expireSeconds 超时时间
     * @return 是否成功
     */
    boolean lock(String lockName, long expireSeconds);

    /**
     * 解锁
     *
     * @param lockName 锁名称
     */
    void release(String lockName);
}
