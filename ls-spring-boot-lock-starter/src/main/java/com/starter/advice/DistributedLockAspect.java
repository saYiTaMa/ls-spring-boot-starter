package com.starter.advice;

import com.starter.annotation.DistributedLock;
import com.starter.service.DistributedLockService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 分布式锁拦截器
 *
 * @author guanxiaop
 * @date 2022/1/26 11:45
 */
@Slf4j
@Aspect
@Component
public class DistributedLockAspect {

    @Resource
    private DistributedLockService distributedLockService;

    /**
     * 切点为注解：标注切点为自定义注解，这样在使用注解的时候，就可以切入
     */
    @Pointcut("@annotation(com.starter.annotation.DistributedLock)")
    public void distributedLock() {
    }


    @Around("@annotation(distributedLock)")
    private Object around(ProceedingJoinPoint joinPoint, DistributedLock distributedLock) throws Throwable {
        log.info("[开始]执行RedisLock环绕通知,获取Redis分布式锁开始");
        // 超时时间
        long timeout = distributedLock.timeout();
        // 锁名称
        String lockName = distributedLock.value();

        if (distributedLockService.lock(lockName, timeout)) {
            try {
                log.info("获取Redis分布式锁[成功]，加锁完成，开始执行业务逻辑...");
                Object proceed = joinPoint.proceed();
                return proceed;
            } catch (Exception e) {
                log.error("获取Redis分布式锁[异常]，加锁失败", e);
                throw e;
            } finally {
                distributedLockService.release(lockName);
                log.info("释放Redis分布式锁[成功]，解锁完成，结束业务逻辑...");
            }
        } else {
            log.error("获取Redis分布式锁[失败]");
        }
        log.info("[结束]执行RedisLock环绕通知");
        return null;
    }
}
