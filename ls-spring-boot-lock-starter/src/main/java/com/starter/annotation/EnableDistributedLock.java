package com.starter.annotation;

import com.starter.configuration.DistributedAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author guanxiaop
 * @date 2022/1/26 14:20
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(DistributedAutoConfiguration.class)
public @interface EnableDistributedLock {
}
