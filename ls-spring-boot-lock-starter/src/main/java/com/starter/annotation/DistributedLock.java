package com.starter.annotation;

import java.lang.annotation.*;

/**
 * 分布式锁
 *
 * @author guanxiaop
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface DistributedLock {

    /**
     * 默认加锁的名称
     */
    String value() default "redis:lock";

    /**
     * 锁超时时间，默认10秒
     */
    long timeout() default 10000L;
}
