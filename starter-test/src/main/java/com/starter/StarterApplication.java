package com.starter;

import com.starter.annotation.EnableDistributedLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guanxiaop
 * @date 2022/1/26 14:04
 */
@SpringBootApplication
@EnableDistributedLock
public class StarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarterApplication.class, args);
    }

}
