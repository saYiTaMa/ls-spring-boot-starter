package com.starter.lock.controller;

import com.starter.annotation.DistributedLock;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guanxiaop
 * @date 2022/1/26 14:07
 */
@RestController
public class DistributionLockController {


    @GetMapping("/lock")
    @DistributedLock(value = "guan",timeout = 10)
    public String lockTest(){
        System.out.println("in");
       return "hello lock";
    }
}
