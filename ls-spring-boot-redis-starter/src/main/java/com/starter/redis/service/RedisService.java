package com.starter.redis.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 基于spring和redis的redisTemplate工具类
 *      针对所有的hash 都是以h开头的方法
 *      针对所有的Set 都是以s开头的方法
 *      针对所有的List 都是以l开头的方法
 *
 * @author guanxiaop
 * @date 2022/1/14 18:32
 */
public interface RedisService {
    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 失效时间
     * @return 是否设置成功
     */
    boolean expire(String key, long time);

    /**
     * 指定缓存在xx时间失效
     *
     * @param key        键
     * @param expireTime 失效时间
     * @return 是否设置成功
     */
    boolean expireAt(String key, Date expireTime);

    /**
     * 判断缓存中是否含有key
     *
     * @param key 键
     * @return 是否包含key
     */
    boolean hasKey(String key);

    /**
     * 批量删除缓存
     *
     * @param key 键
     */
    void del(String... key);

    /**
     * 根据key获取缓存
     *
     * @param key 键
     * @return 缓存
     */
    Object get(String key);

    /**
     * 根据key获取缓存
     *
     * @param key   键
     * @param clazz 类型
     * @return 缓存
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * 普通存入缓存数据
     *
     * @param key   键
     * @param value 值
     * @return 是否缓存成功
     */
    boolean set(String key, Object value);

    /**
     * 普通存入缓存，并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  缓存时间
     * @return 是否缓存成功
     */
    boolean set(String key, Object value, long time);

    /**
     * 不存在时赋值 value 并设置过期时间
     *
     * @param key   键
     * @param value 值
     * @param time  缓存时间
     * @return 是否缓存成功
     */
    boolean setex(String key, Object value, long time);

    /**
     * 递增
     *
     * @param key   键
     * @param delta 递增因子
     * @return 结果
     */
    long incr(String key, long delta);

    /**
     * 递增
     *
     * @param key   键
     * @param delta 递增因子
     * @param time  过期时间
     * @return 结果
     */
    long incr(String key, long delta, long time);

    /**
     * 递减
     *
     * @param key   键
     * @param delta 递减因子
     * @return 结果
     */
    long decr(String key, long delta);

    /**
     * 递减
     *
     * @param key   键
     * @param delta 递减因子
     * @param time  过期时间
     * @return 结果
     */
    long decr(String key, long delta, long time);

    /**
     * hashget
     *
     * @param key  键
     * @param item 字段
     * @return 对象
     */
    Object hget(String key, String item);

    /**
     * 获取hashkey对应的所有键值
     *
     * @param key 键
     * @return 键-值
     */
    Map<Object, Object> hmget(String key);

    /**
     * hashset
     *
     * @param key 键
     * @param map 值
     * @return 是否添加成功
     */
    boolean hmset(String key, Map<String, Object> map);

    /**
     * hashset 并设置时间
     *
     * @param key  键
     * @param map  值
     * @param time 过期时间
     * @return 是否添加成功
     */
    boolean hmset(String key, Map<String, Object> map, long time);

    /**
     * 向一张表中放入数据，如果不存在就创建
     *
     * @param key   键
     * @param item  字段
     * @param value 值
     * @return 是否添加成功
     */
    boolean hset(String key, String item, Object value);

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  字段
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return
     */
    boolean hset(String key, String item, Object value, long time);

    /**
     * 删除hash表中的值
     *
     * @param key  键
     * @param item 字段
     */
    void hdel(String key, Object... item);

    /**
     * 判断hash表里是否有改key值
     *
     * @param key  键
     * @param item 字段
     * @return 是否包含
     */
    boolean hHasKey(String key, String item);

    /**
     * hash递增，如果不存在就会创建一个，并把新增的值返回
     *
     * @param key  键
     * @param item 字段
     * @param by   浮动因子
     * @return 结果
     */
    double hincr(String key, String item, double by);

    /**
     * hash递减，如果不存在就会创建一个，并把减少的值返回
     *
     * @param key  键
     * @param item 字段
     * @param by   浮动因子
     * @return 结果
     */
    double hdecr(String key, String item, double by);

    /**
     * 根据key获取set中的所有值
     *
     * @param key 键
     * @return 值
     */
    Set<Object> sGet(String key);

    /**
     * 根据value从一个set中查询是否存在
     *
     * @param key   键
     * @param value 值
     * @return 是否存在
     */
    boolean sHasKey(String key, Object value);

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param values 值
     * @return 数量
     */
    long sSet(String key, Object... values);

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间
     * @param values 值
     * @return 数量
     */
    long sSetAndTime(String key, long time, Object... values);

    /**
     * 获取set缓存数据的长度
     *
     * @param key 键
     * @return 数据长度
     */
    long sGetSetSize(String key);

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 来时
     * @param end   结束
     * @return 值
     */
    List<Object> lGet(String key, long start, long end);

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return 长度
     */
    long lGetListSize(String key);

    /**
     * 通过索引 获取list缓存中的值
     *
     * @param key   键
     * @param index 索引
     * @return 值
     */
    Object lGetIndex(String key, long index);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return 是否成功
     */
    boolean lSet(String key, Object value);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  存活时间
     * @return 是否成功
     */
    boolean lSet(String key, Object value, long time);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return 是否成功
     */
    boolean lSet(String key, List<Object> value);

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  存活时间
     * @return 是否成功
     */
    boolean lSet(String key, List<Object> value, long time);

    /**
     * 根据索引 修改list缓存中的某个数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return 是否成功
     */
    boolean lUpdateIndex(String key, long index, Object value);

    /**
     * 移除n个值为value
     *
     * @param key   键
     * @param count 数量
     * @param value 值
     * @return 数量
     */
    long lRemove(String key, long count, Object value);

    /**
     * 正则匹配key
     *
     * @param pattern 规则
     * @return 键
     */
    Set<String> keys(String pattern);

    /**
     * 更具正则表达式删除keys
     *
     * @param pattern 规则
     * @return 是否删除成功
     */
    boolean deleteByPattern(String pattern);
}
